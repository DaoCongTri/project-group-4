/** @format */

const storageServices = {
  setDataProduct: (dataProduct) => {
    if (typeof sessionStorage !== "undefined") {
      sessionStorage.setItem(
        keyStorageProductCart,
        JSON.stringify(dataProduct)
      );
    } else {
      console.error("sessionStorage is not available.");
    }
  },
  getDataProduct: () => {
    if (typeof sessionStorage !== "undefined") {
      const dataJsonProduct = JSON.parse(
        sessionStorage.getItem(keyStorageProductCart)
      );
      return dataJsonProduct;
    } else {
      console.error("sessionStorage is not available.");
      return null;
    }
  },
  removeDataProduct: () => {
    if (typeof sessionStorage !== "undefined") {
      sessionStorage.removeItem(keyStorageProductCart);
    } else {
      console.error("sessionStorage is not available.");
    }
  },
  removeTotalCart: () => {
    if (typeof sessionStorage !== "undefined") {
      sessionStorage.removeItem("total");
    } else {
      console.error("sessionStorage is not available.");
    }
  },
  setDataUser: (dataUser) => {
    if (typeof Storage !== "undefined") {
      sessionStorage.setItem(keyStorageUser, JSON.stringify(dataUser));
    } else {
      console.error("sessionStorage is not available.");
    }
  },
  getDataUser: () => {
    if (typeof sessionStorage !== "undefined") {
      const dataJsonUser = JSON.parse(sessionStorage.getItem(keyStorageUser));
      return dataJsonUser;
    } else {
      console.error("sessionStorage is not available.");
      return null;
    }
  },
  removeDataUser: () => {
    if (typeof sessionStorage !== "undefined") {
      sessionStorage.removeItem(keyStorageUser);
    } else {
      console.error("sessionStorage is not available.");
    }
  },
  setTotalCart: (total) => {
    if (typeof Storage !== "undefined") {
      sessionStorage.setItem("total", total);
    } else {
      console.error("sessionStorage is not available.");
    }
  },
  getTotalCart: () => {
    if (typeof sessionStorage !== "undefined") {
      const total = sessionStorage.getItem("total") * 1;
      return total;
    } else {
      console.error("sessionStorage is not available.");
      return null;
    }
  },
  removeTotalCart: () => {
    if (typeof sessionStorage !== "undefined") {
      sessionStorage.removeItem("total");
    } else {
      console.error("sessionStorage is not available.");
    }
  },
};
