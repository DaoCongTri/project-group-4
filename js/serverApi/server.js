var cartItemService = {
    getList: () => {
        return axios({
            url: `${productCartAPI}`,
            method: "GET",
        });
    },
    remove: (id) => {
        return axios({
            url: `${productCartAPI}/${id}`,
            method: "DELETE",
        });
    },
    create: (dataItem) => {
        return axios({
            url: productCartAPI,
            method: "POST",
            data: dataItem,
        });
    },
    update: (id, dataItem) => {
        return axios({
            url: `${productCartAPI}/${id}`,
            method: "PUT",
            data: dataItem,
        });
    },
};
