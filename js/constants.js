/** @format */

// const userAPI = "https://64413df4792fe886a8a23776.mockapi.io/users";
// const productAPI = "https://64413df4792fe886a8a23776.mockapi.io/product";
const productCartAPI = "https://65f7e970b4f842e8088657e8.mockapi.io/carts";
const keyStorageProduct = "DATA_API_PRODUCT";
const keyStorageUser = "DATA_API_USER";
const keyStorageInfoUser = "DATA_INFO_USER";
const keyStorageProductCart = "DATA_PRODUCT_CART";
const listProduct = [
  {
    name: "NINJA 400 ABS KRT EDITION",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4931/0923d9d0-7ea2-4403-843b-c74d130086c6.png?w=675",
    price: "171.600.000",
    detail:
      "Kawasaki Ninja 400 ABS 2021 vẫn được trang bị khối động cơ 2 xy-lanh, dung tích 399 cc, làm mát bằng dung dịch. Hộp số được trang bị hệ thống Chống trượt ly hợp (Slipper Clutch) giúp chống khóa bánh khi dồn số gấp. Công suất ấn tượng với mức cực đại đạt 44,8 mã lực tại vòng tua máy 10.000 vòng/phút và mô-men xoắn cực đại 37 Nm tại 8.000 vòng/phút. Bình xăng có dung tích 14 lít, giúp xe có thể đi được quãng đường 338 km với bình xăng đầy. Trọng lượng ướt của xe là 168 kg, nhẹ hơn Ninja 300 đến 8 kg, giúp mẫu xe này trở thành mẫu xe có tỉ lệ công suất / khối lượng thuộc hàng tốt nhất trong phân khúc. Hệ thống phanh ABS 2 kênh là trang bị tiêu chuẩn. Ngoài ra, mẫu Sportbike tầm trung này còn sỡ hữu những tính năng thú vị, góp phần cải thiện trải nghiệm khi lái xe. Điển hình như chỉ số ECO hỗ trợ giám sát mức tiêu hao nhiên liệu với độ chính xác cao và tính năng ERGO-FIT giúp người lái tìm được vị trí lái phù hợp nhất và thoải mái nhất.",
    color: "XANH LIME / EBONY",
    type: "NINJA",
    id: 1,
  },
  {
    name: "NINJA 650 ABS KRT EDITION",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4937/bb352fce-1309-4037-8e3c-924bb7141c4d.jpg",
    price: "210.000.000",
    detail:
      "Được sinh ra để tiếp tục làm rạng danh dòng mô tô thể thao Ninja, mẫu xe Kawasaki Ninja 650 mới mang trong mình động cơ 649 cm³ khoẻ khoắn, cùng hàng loạt những công nghệ hiện đại và những thay đổi ấn tượng về thiết kế. Hiệu suất tuyệt vời của một chiếc mô tô thể thao kết hợp với tư thế lái thẳng mang lại sự thú vị cho những chuyến đi hàng ngày, trong khi dáng vẻ đỉnh cao của mẫu xe khiến nó trở nên như một huyền thoại.",
    color: "Lime Green / Ebony",
    type: "NINJA",
    id: 2,
  },
  {
    name: "NINJA ZX-25R SE KRT",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4930/b53c758c-ef2d-4489-a23c-f0aec2d2a6e5.jpg",
    price: "197.300.000",
    detail:
      "Về sức mạnh, Kawasaki Ninja ZX-25R ABS 2020 sử dụng khối động cơ DOHC 4 xy-lanh, làm mát bằng dung dịch, dung tích 249,8 cc. Ở thời điểm hiện tại, đây là mẫu xe 250 phân khối duy nhất được trang bị khối động cơ 4 xy-lanh (4 máy). Động cơ trên giúp sản sinh công suất tối đa lên đến 50 mã lực (với Ram Air) tại 15.500 vòng/phút. Mô-men xoắn cực đại đạt 22,9 Nm tại vòng tua máy 7.500 vòng/phút. Hộp số côn tay 6 cấp. Tốc độ tối đa của xe theo công bố, đạt đến 197 km/h, nhanh nhất trong phân khúc.",
    color: "Xanh Lime / Ebony",
    type: "NINJA",
    id: 3,
  },
  {
    name: "NINJA ZX-10R ABS KRT EDITION",
    image:
      "https://rebel.vn/uploads/file//Kawasaki%20Ninja/kawasaki-ninja-zx-10r-abs-krt-edition-chinh-hang-sieu-xe-the-thao5000.jpg",
    price: "729.000.000",
    detail:
      "Ninja ZX-10R 2022 được trang bị khối động cơ 998 cc, 4 xi-lanh thẳng hàng, DOHC, 16 van, làm mát bằng nước. Công suất tối đa 200 mã lực tại dải tua 13.200 vòng/phút và mô-men xoắn tối đa 114,9 Nm tại 11.400 vòng/phút, tốc độ tối đa 299 km/h, tăng tốc 0-100 km/h trong 2,9 giây, hộp số 6 tốc độ. Bảng đồng hồ điện tử Kawasaki Ninja ZX-10R 2022 đã được nâng cấp lên màn hình màu TFT LCD hiển thị đầy đủ các thông số kỹ thuật như ODO, các trip chạy và các Mode vận hành trên xe như Sport, Road, Rain và Rider.. Ngoài ra, ZX-10R 2022 còn có chức năng kết nối với điện thoại thông minh cũng như kiểm soát hành trình. Đánh giá chung đây là con xe đáng mua trong phân khúc 1000cc với số tiền cũng rất vừa phải với một em sportbike đầy đủ công nghệ như này.",
    color: "XANH LIME / EBONY / TRẮNG NGỌC TRAI BLIZZARD",
    type: "NINJA",
    id: 4,
  },
  {
    name: "NINJA H2R",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4915/cc471aa1-aa2d-4126-92c8-9ca6b9c6c005.png?w=675",
    price: "1.875.000.000",
    detail:
      "Kawasaki Ninja H2R được biết đến là mẫu siêu mô tô đang nắm giữ kỷ lục nhanh nhất thế giới từ năm 2016 cho tới nay mà vẫn chưa có một siêu mô tô nào có thể phá vỡ. 'Ma vương tốc độ' H2R chỉ mất 26 giây để đạt vận tốc từ 0-400 km/h trên một cây cầu ở Thổ Nhĩ Kỳ. Ngay cả những chiếc siêu xe ô tô nổi tiếng thế giới mang trong mình khối động cơ cực khủng như Lamborghini Aventador hay Ferrari 458 Italia cũng chưa thể đạt đến con số này nếu không được độ lại, điều này khiến nhiều người phải choáng váng. Tuy nhiên, mẫu siêu mô tô chỉ được phép sử dụng trên đường đua bởi nhiều chi tiết của xe làm bằng sợi carbon nhằm giảm trọng lượng. Dàn áo bên ngoài cũng được làm từ vật liệu carbon, theo Kawasaki, phải mất đến 8 tháng để hoàn thành sản xuất riêng phần dàn áo cho xe. Siêu mô tô này cũng đã bị loại bỏ các trang bị cần thiết của một mẫu xe thương mại lưu thông trên đường phố như đèn, còi, xi-nhan... Trái tim của Kawasaki H2R chính hãng 2021 là khối động cơ siêu nạp, 4 xylanh, dung tích 998cc, kết hợp hệ thống siêu nạp Supercharged giúp xe sản sinh công suất tối đa lên tới 310 mã lực (tăng lên 326 mã lực khi kích hoạt Ram-air) tại 14.000 vòng/phút. Sức kéo cực khủng với mô men xoắn cực đại đạt 155 Nm tại 12.500 vòng/phút.",
    color: "ĐEN ÁNH KIM TRÁNG GƯƠNG",
    type: "NINJA",
    id: 5,
  },
  {
    name: "NINJA ZX-10R ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4922/db942ec5-cf7b-4dec-b0f3-ed7c6ac3f688.jpg",
    price: "765.700.000",
    detail:
      "Ninja ZX-10R 2022 được trang bị khối động cơ 998 cc, 4 xi-lanh thẳng hàng, DOHC, 16 van, làm mát bằng nước. Công suất tối đa 200 mã lực tại dải tua 13.200 vòng/phút và mô-men xoắn tối đa 114,9 Nm tại 11.400 vòng/phút, tốc độ tối đa 299 km/h, tăng tốc 0-100 km/h trong 2,9 giây, hộp số 6 tốc độ. Bảng đồng hồ điện tử Kawasaki Ninja ZX-10R 2022 đã được nâng cấp lên màn hình màu TFT LCD hiển thị đầy đủ các thông số kỹ thuật như ODO, các trip chạy và các Mode vận hành trên xe như Sport, Road, Rain và Rider.. Ngoài ra, ZX-10R 2022 còn có chức năng kết nối với điện thoại thông minh cũng như kiểm soát hành trình. Đánh giá chung đây là con xe đáng mua trong phân khúc 1000cc với số tiền cũng rất vừa phải với một em sportbike đầy đủ công nghệ như này.",
    color: "Trắng Ngọc Trai / Đen Diablo",
    type: "NINJA",
    id: 6,
  },
  {
    name: "Z400 ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4939/5ec71623-0f32-48df-802b-8a7fc91ae54e.jpg?w=675",
    price: "164.000.000",
    detail:
      "Kawasaki Z400 ABS 2020 sử dụng khối động cơ loại 2 xy-lanh song song, tổng dung tích 399 cc tương tự như mẫu Ninja 400, trục cam đôi DOHC, có phun xăng điện tử và làm mát bằng dung dịch. Động cơ này sản sinh công suất tối đa đạt 44,8 mã lực tại vòng tua máy 10.000 vòng/phút và mô-men xoắn cực đại ở mức 38 Nm tại 8.000 vòng/phút. Hộp số côn tay 6 cấp có tính năng ly hợp chống trượt bánh sau Assist & Slipper Clutch. Bình xăng của Kawasaki Z400 ABS có dung tích 14 lít, lớn hơn đáng kể so với các mẫu xe đối thủ cùng phân khúc. Mức tiêu hao nhiên liệu theo công bố từ hãng là 4,4 L/100 km, cho khả năng vận hành tối ưu trên những đoạn đường dài.",
    color: "XANH CANDY LIME / ĐEN ÁNH KIM",
    type: "Z",
    id: 7,
  },
  {
    name: "Z650 ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4887/f7a0519e-b7d2-4ec3-bb7e-47d16f3a7283.jpg",
    price: "194.000.000",
    detail:
      "Trái tim của Kawasaki Z650 ABS 2020 là khối động cơ có dung tích 649 cc, 2 xy-lanh, đường kính piston 83 mm. Khối động cơ này sản sinh công suất tối đa 67,3 mã lực tại vòng tua máy 8.000 vòng/phút và mô-men xoắn cực đại 64 Nm tại 6.700 vòng/phút. Bên trong được trang bị hệ thống van tiết lưu kép cho phép động cơ tăng tốc nhanh và mượt mà. Xe cũng được hỗ trợ công nghệ chống trượt khi dồn số gấp Slipper Clutch, giúp giữ cho bánh sau bám đường khi giảm tốc độ đột ngột để tránh hiện tượng bánh xe bị nhảy hoặc trượt. Xe sử dụng phanh đĩa đôi đường kính 300 mm ở phía trước, và phanh đĩa đơn đường kính 220 mm ở phía sau. Cả 2 đều được trang bị hệ thống chống bó cứng phanh ABS (Antilock Brake System). Cặp lốp trước sau cũng được Kawasaki nâng cấp với việc sử dụng bộ lốp Dunlop Sportmax Roadsport 2, kích thước lốp trước 120/70-17, sau 160/60-17, giúp Z650 ABS 2020 vào cua ổn định hơn kể cả ở tốc độ cao. Bình xăng xe có thiết kế khỏe khoắn hơn với các đường gân chạy dọc, dung tích bình xăng 15 lít, tiêu hao nhiên liệu ở mức khá tốt, khoảng 4,9 L/100km.",
    color: "Xám Graphene Steel / Đen Tuyền",
    type: "Z",
    id: 8,
  },
  {
    name: "Z900 ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4910/d0532fbf-df57-42a4-944e-4c8714252a5f.png?w=675",
    price: "324.900.000",
    detail:
      "Trái tim của Kawasaki Z900 ABS 2020 là khối động cơ DOHC với 16 van, 4 xy-lanh thẳng hàng, dung tích 948 cc. Sản sinh công suất tối đa 123.6 mã lực tại vòng tua máy 9,500 vòng/phút. Mô men xoắn cực đại ở 98.6 Nm tại 7,700 vòng/phút. Xe cũng được hỗ trợ công nghệ chống trượt khi dồn số gấp Slipper Clutch, giúp giữ cho bánh sau bám đường khi giảm tốc độ đột ngột để tránh hiện tượng bánh xe bị nhảy hoặc trượt. Trái piston trong động cơ của Z900 ABS 2020 sử dụng phương pháp đúc nhiệt dưới áp lực cao, giúp giảm trọng lượng và bền ngang piston rèn. Xe sử dụng màn hình hiển thị mới được nâng cấp thành dạng LCD, cho cảm giác hiện đại, dễ nhìn và tươi mới hơn. Màn hình mới hỗ trợ hiển thị các thông tin như tốc độ, vòng tua máy, thông tin về các chuyến đi và quãng đường đi hiện tại. Nâng cấp đắt giá nhất chính là khả năng tùy chọn về chế độ vận hành gồm Full Power và Half-Power. Cùng với đó là 3 lựa chọn chế độ lái gồm có: Sport, Road cùng Rain, giúp người lái đạt được những trải nghiệm vận hành an toàn, phù hợp nhất.",
    color: "XANH LIME CANDY / ĐEN ÁNH KIM",
    type: "Z",
    id: 9,
  },
  {
    name: "Z1000R Edition ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4921/caded9a1-748e-4ed7-b435-8fe1437b5f70.png?w=675",
    price: "514.600.000",
    detail:
      "Z1000R 2022 vẫn giữ thiết kế theo ngôn ngữ Sumogi đầy mạnh mẽ với những đường cong cùng nhiều góc cạnh được kết hợp một cách tinh tế đến đẹp mắt. Về kích thước, Z1000R 2022 không thay đổi so với thế hệ trước chiều dài 2.045 mm, chiều rộng 790 mm, chiều cao 1.055 mm, chiều cao yên 815 mm và trọng lượng không tải 221 kg. Z1000R ABS 2022 vẫn sở hữu nhiều tính năng an toàn tiên tiến, nổi bật như hệ thống chống bó cứng phanh ABS trên cả hai bánh, phanh đĩa đôi, đường kính 310 mm phía trước. 'Trái tim' của Kawasaki Z1000R 2022 vẫn là khối động cơ 4 xi-lanh, dung tích 1.043cc, sản sinh công suất tối đa 142 mã lực tại 10.000 vòng/phút và mô-men xoắn cực đại 111 Nm tại 7.300 vòng/phút. Xe sở hữu trọng lượng không tải 221 kg. Ngoài ra, hãng Kawasaki còn hiệu chỉnh hệ thống điều khiển động cơ giúp Z1000R 2022 mới tăng tốc nhanh và đạt tốc độ cao hơn.",
    color: "XÁM CARBON / XANH NGỌC LỤC BẢO / ĐEN DIABLO",
    type: "Z",
    id: 10,
  },
  {
    name: "Z650RS ABS",
    image:
      "https://motorrock.com.vn/uploads/file//1%20AKAW/kawasaki%20Z650RS%202023%204.jpg",
    price: "233.000.000",
    detail:
      "Z650RS sử dụng chung bộ động cơ với những người anh em cùng phân khúc 650cc của Kawasaki, bao gồm Z650, Ninja 650, Vulcan 650 và Versys 650. Khối động cơ 2 xylanh DOHC 649cc làm mát bằng dung dịch có thể sản sinh ra công suất lớn nhất 68 mã lực và mômen xoắn cực đại 64 Nm. Bướm ga kép cho độ phản hồi mượt mà khi lên ga, với hộp số 6 cấp, và đặc biệt là được trang bị bộ nồi chống trượt Assist & Slipper Clutch. Bộ động cơ này tạo ra những âm thanh gầm gừ, cuốn hút ở những dải vòng tua thấp và trung. Bộ động cơ này được đánh giá là đủ mạnh nhưng không quá khó điều khiển. Những người mới bắt đầu cũng có thể dễ dàng kiểm soát chiếc xe, và những tay lái kỳ cựu vẫn cảm nhận được sự phấn khích khi vặn ga. Với động cơ 2 xylanh dung tích 649cc, Z650RS mang lại một độ bốc ấn tượng ở những nước ga đầu và cả những dải vòng tua giữa. Vì vậy, chiếc xe này đề cao tính thực dụng, vừa có thể sử dụng trong phố, lượn lờ cafe và cũng có thể sử dụng đi những chuyến tour ngắn ngày.",
    color: "Xanh Ngọc Lục Bảo",
    type: "Z",
    id: 11,
  },
  {
    name: "Z H2 SE",
    image: "https://vcdn-vnexpress.vnecdn.net/2022/09/26/-6953-1664147200.jpg",
    price: "789.300.000",
    detail:
      "Mẫu nakedbike đầu bảng của Kawasaki lắp động cơ siêu nạp gần 1.000 phân khối, công suất 200 mã lực. Cung cấp sức mạnh cho Kawasaki Z H2 SE (trọng lượng ướt 240 kg) là cỗ máy DOHC siêu nạp 4 xi-lanh thẳng hàng, dung tích 998 phân khối, làm mát bằng dung dịch. Động cơ này cho công suất 200 mã lực tại vòng tua 11.000 vòng/phút và mô-men xoắn cực đại 137 Nm tại 8.500 vòng/phút. Hộp số 6 cấp. Những trang bị còn lại trên Kawasaki Z H2 SE vốn quen thuộc ở các dòng môtô cao cấp như phanh đĩa kép bánh trước (đơn bán sau), kẹp phanh Brembo, phuộc hành trình ngược và monoshock phía sau đều có thể điều chỉnh...",
    color: "Đen Diablo / Xanh Golden",
    type: "Z",
    id: 12,
  },
  {
    name: "W800 ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4938/8fbd6e9b-8336-4c05-b55d-400c212cbde1.jpg",
    price: "382.400.000",
    detail:
      "W800 lắp động cơ 4 thì dung tích 773 phân khối, xi-lanh đôi, công suất 47 mã lực tại vòng tua 6.000 vòng/phút, mô-men xoắn cực đại 62,9 Nm tại 4.800 vòng/phút. Hộp số côn tay 6 cấp, làm mát bằng không khí. Kích thước dài, rộng, cao của Kawasaki W800 lần lượt 2.190 mm, 790 mm, 1.075 mm. Bình xăng 15 lít, chiều cao yên 790 mm và trọng lượng 226 kg. Thiết kế gọn, đơn giản, W800 cho phép khả năng tùy biến cao của người dùng. Nhiều bản độ đã được tạo ra từ mẫu xe của Kawasaki. Phuộc trước của Kawasaki W800 loại ống lồng, phía sau loại lò xo giảm chấn. Hai bánh trước/sau đều đi kèm phanh đĩa đơn. Dẫn động bằng xích.",
    color: "Metallic Slate Blue",
    type: "W",
    id: 13,
  },
  {
    name: "W175 SE",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4918/f24ae4a0-3cc3-4ebb-90f7-0cccaab3429d.png?w=675",
    price: "78.000.000",
    detail:
      "Kawasaki W175 SE 2021 được trang bị khối động cơ xy-lanh đơn, dung tích 177 cc, SOHC, làm mát bằng gió. Cho công suất tối đa 13 mã lực tại 7.500 vòng/phút và mô-men xoắn cực đại 13,2 Nm tại 6.000 vòng/phút. Hộp số 5 cấp côn tay. Do sử dụng động cơ có dung tích trên 175 cc nên người sử dụng sẽ cần có bằng lái hạng A2. Dù vậy, điều này lại khiến W175 SE trở thành mẫu xe lý tưởng cho người mới tập chơi xe phân khối lớn. Giúp người lái vừa có cơ hội tập làm quen với dòng xe côn tay, vừa có thể nâng hạng bằng lái cho mục đích sử dụng sau này. Ống xả mang dáng dấp của dòng môtô Anh quốc thập niên 50, phủ sơn đen nhám toàn bộ, hài hòa với màu động cơ. Thiết kế ống xả với phần đuôi được vuốt nhỏ lại và kéo dài trông khá lạ mắt. Nhờ đó, âm thanh cho ra có phần phấn khích và mang nét đặc trưng riêng của dòng xe Kawasaki.",
    color: "XANH COVERT",
    type: "W",
    id: 14,
  },
  {
    name: "VULCAN S",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4879/9119883e-f3e3-4e18-ab12-1f04080be94d.png?w=675",
    price: "241.000.000",
    detail:
      "Kawasaki Vulcan S 2020 được trang bị khối động cơ dung tích 649 cc, hai xi-lanh song song, làm mát bằng nước. Đây là khối động cơ được tùy biến từ Ninja 650. Cỗ máy DOHC 8 van với cam được sửa đổi, kết hợp với hệ thống phun xăng điện tử giúp giảm lượng khí thải. Bánh đà được tăng thêm 28% khối lượng so với động cơ trên Ninja 650 giúp xe đầm và mượt hơn. Xe được trang bị hộp số 6 tốc độ. Vulcan S 2020 mang tới thông số momen xoắn cực ấn tượng với con số 63 Nm tại 6.600 vòng/phút. Có thể nói, đây là mẫu xe sở hữu khả năng tăng tốc ấn tượng nhất phân khúc. Công suất cực đại của xe đạt mức 60 mã lực tại vòng tua máy 7.500 vòng/phút. Khối động cơ trên xe được đánh giá khá mượt mà và chuẩn mực. Mang đến cảm giác lái nhẹ nhàng và cũng không kém phần phấn khích mỗi khi vặn tay ga. Thùng xăng dung tích 14 lít được thiết kế thuôn gọn. Cụm đồng hồ được thiết kế hiện đại và trực quan, với các nút bấm đơn giản, dễ thao tác. Bao gồm một đồng hồ cơ hiển thị vòng tua máy và một màn hình LCD hiển thị các thống số cơ bản khác.",
    color: "ĐEN MỜ ÁNH KIM",
    type: "VULCAN",
    id: 15,
  },
  {
    name: "VULCAN S ABS",
    image:
      "https://content2.kawasaki.com/ContentStorage/KMV/Products/4906/0182c722-bbe1-4f26-bb91-040650207f26.png?w=675",
    price: "253.000.000",
    detail:
      "Kawasaki Vulcan S 650 2023 được trang bị động cơ parallel-twin dung tích 649cc, DOHC, 8 van, sản sinh công suất tối đa 59,9 mã lực và mô-men xoắn cực đại 52,4 Nm. Bên cạnh đó động cơ này còn được trang bị hệ thống làm mát bằng chất lỏng và hệ thống phun xăng điện tử thế hệ mới để đảm bảo hiệu suất hoạt động tốt nhất. Với động cơ mạnh mẽ và hiệu quả, Kawasaki Vulcan S 650 2023 cho phép người lái cảm nhận được sự khởi động nhanh chóng và tăng tốc mạnh mẽ, đồng thời cũng mang lại trải nghiệm lái êm ái và tiết kiệm nhiên liệu.",
    color: "XÁM GRAPHENE STEEL",
    type: "VULCAN",
    id: 16,
  },
];
const listUser = [
  {
    name: "Tri",
    email: "tri@gmail.com",
    password: "1234",
    userId: "1",
  },
  {
    name: "Thuong",
    email: "thuong@gmail.com",
    password: "1234",
    userId: "2",
  },
  {
    name: "Quoc",
    email: "quoc@gmail.com",
    password: "1234",
    userId: "3",
  },
];
const listMember = [
  {
    id: 1,
    name: "Đào Công Trí",
    mssv: "23-0-00190",
    email: "daocongtri20031609<br>@gmail.com",
    avatar: "../image/daocongtri.jpg",
  },
  {
    id: 2,
    name: "Trần Khắc Thương",
    mssv: "23-0-00149",
    email: " tt4060779@gmail.com",
    avatar: "../image/trankhacthuong.jpg",
  },
  {
    id: 3,
    name: "Đồng Minh Quốc",
    mssv: "23-0-00285",
    email: "dongminhquoc162005<br>@gmail.com",
    avatar: "../image/dongminhquoc.jpg",
  },
];
