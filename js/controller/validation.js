/** @format */

var showMessage = (id, message) => {
  document.getElementById(id).innerHTML = message;
};
var checkNull = (idErr, value) => {
  if (value.length == 0 || value == 0) {
    showMessage(idErr, "Vui lòng nhập giá trị");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
