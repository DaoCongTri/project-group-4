/** @format */

// render special product
const renderSpecialProduct = (array) => {
  let productList = array;
  productList = productList.slice(0, 4).map((item, index) => {
    return `
        <input id="slide-dot-${index + 1}" type="radio" name="slides" ${
      index + 1 == 1 ? "checked" : ""
    }  />
        <div class="slide slide-${index + 1}" onclick="DetailPage('${
      item.id
    }')">
          <img src=${item.image} alt="">
          <p>${item.name}</p>
        </div>
        `;
  });
  productList = productList.join("");
  return productList;
};
// render product list home page
const renderProductListHomePage = (array) => {
  let productList = array;
  productList = productList.slice(4).map((item) => {
    return `
            <div id="itemCart" key=${item.id} class="itemHidden" onclick="DetailPage('${item.id}')">
                <p class="color-changing-text">Mới</p>
                <img src=${item.image} alt=${item.type}>
                <p>${item.name}</p>
                <p>Giá Bán: ${item.price}đ</p>
            </div>
        `;
  });
  productList = productList.join("");
  return productList;
};

// render product list
const renderProductList = (array) => {
  let productList = array;
  productList = productList.map((item) => {
    return `
            <div id="itemCart" key=${item.id} onclick="DetailPage('${item.id}')">
                <p class="color-changing-text">Mới</p>
                <img src=${item.image} alt=${item.type}>
                <p>${item.name}</p>
                <p>Giá Bán: ${item.price}đ</p>
            </div>
        `;
  });
  productList = productList.join("");
  return productList;
};

// render product cart
const renderProductCart = (cart) => {
  let total = 0;
  let contentCart = cart;
  if (!cart) {
    // document.getElementById("totalPrice").innerHTML = 0 + " VND";
    return;
  } else {
    contentCart = contentCart.map((item) => {
      const totalPrice =
        item.quantity *
          (parseFloat(item.price.replace(/\./g, "")) -
            parseFloat(item.price.replace(/\./g, "")) * 0.1) +
        1000000;
      total += totalPrice;
      console.log(total);
      return `
            <div id="cartList">
          <div id="cartImage">
            <img
              src=${item.image}
              alt=${item.name} />
            <button onclick="removeCart(${item.id})" data-name="${item.name}">
                  Xóa
            </button>
          </div>
          <div id="cartInfo">
            <h1>${item.name}</h1>
            <p>
              Số lượng:
              <button
                type="button"
                onclick="upDownQuanlity('down', ${item.id})">
                <i class="w-5 fa-solid fa-minus fa-lg"></i>
              </button>
              <span>${item.quantity}</span>
              <button
                type="button"
                onclick="upDownQuanlity('up', ${item.id})">
                <i class="w-5 fa-solid fa-plus fa-lg"></i>
              </button>
              * <span style="color: red; font-weight: 600">${item.price}đ - 10%</span>
            </p>
            <p>Phí Giao Hàng: <span style="color: red; font-weight: 600">1.000.000đ</span></p>
            <p>Tổng: <span style="color: red; font-weight: 600">${totalPrice}đ</span></p>
          </div>
        </div>
    `;
    });
  }
  storageServices.setTotalCart(total);
  contentCart = contentCart.join("");
  return contentCart;
};

// render group member

const renderGroupMember = () => {
  let groupMember = listMember;
  groupMember = groupMember.map((member) => {
    return `
      <div id="aboutMember">
        <img src=${member.avatar} alt="">
        <h1>${member.name}</h1>
        <p><strong>MSSV:</strong> ${member.mssv}</p>
        <p><strong>Email:</strong> ${member.email}</p>
      </div>
    `;
  });
  groupMember = groupMember.join("");
  return groupMember;
};

// event filter product type page base on menu drop down
var menuItems = document.querySelectorAll(".menuItem");
menuItems.forEach(function (item) {
  item.addEventListener("click", async function () {
    var value = this.getAttribute("id");
    const dataValue = listProduct.filter((obj) => obj.type === value);
    console.log(dataValue);
    document.getElementById("body").innerHTML = `
                <section id="productCart" class="container">
                  <h1 id="label">CÁC MẪU XE ${value}</h1>
                  <div id="proBtnSort" style="text-align:right;">
                    <button type="button" onclick="sortPriceProduct('${value}', 'ascending')">
                      <i class="fa-solid fa-arrow-up-wide-short"></i>
                    </button>
                    <button type="button" onclick="sortPriceProduct('${value}', 'descending')">
                      <i class="fa-solid fa-arrow-down-short-wide"></i>
                    </button>
                  </div>
                  <div id="listCart" class="array1">
                    ${renderProductList(dataValue)}
                  </div>
                  <div id="listCart" class="array2">
                  </div>
                </section>
            `;
  });
});

// event handle navbar account change
const handleNavbarAccountChange = () => {
  const dataUserAccount = storageServices.getDataUser();
  const navbarAccount = dataUserAccount
    ? `
    <button type="button" id="accountUser" onclick="show_closeDropDownNavbarAccount()">${dataUserAccount.name}</button>
    <div id="headerAccountDropdown" class="closeBtn">
      <h1>Thông Tin Quý Khách</h1>
      <p>Họ và Tên: ${dataUserAccount.name}</p>
      <p>Email: ${dataUserAccount.email}</p>
      <button type="button" onclick="handleLogout()">Đăng Xuất</button>
    </div>
    `
    : `<a href="#login" onclick="LoginPage()">Login</a>`;
  const navCollapseAccount = dataUserAccount
    ? `
    <button type="button" onclick="show_closeDropDownNavbarAccount()">Chào mừng ${dataUserAccount.name}</button>
    <button type="button" onclick="handleLogout()">Đăng Xuất</button>
    `
    : `<a href="#login" onclick="LoginPage()">Login</a>`;
  document.getElementById("headNavAccount").innerHTML = navbarAccount;
  document.getElementById("headNavCollapseAccount").innerHTML =
    navCollapseAccount;
};
handleNavbarAccountChange();
// event handle logout
const handleLogout = () => {
  storageServices.removeDataUser();
  alert("Bạn đã đăng xuất thành công!");
};
// show / close dropdown account
const show_closeDropDownNavbarAccount = () => {
  var x = document.getElementById("headerAccountDropdown");
  if (x.className.indexOf("showBtn") == -1) {
    x.className = x.className.replace("closeBtn", "showBtn");
  } else {
    x.className = x.className.replace("showBtn", "closeBtn");
  }
};
// show / close dropdown
const show_closeDropDown = () => {
  var x = document.getElementById("itemMenuDropDown");
  if (x.className.indexOf("showBtn") == -1) {
    x.className = x.className.replace("closeBtn", "showBtn");
  } else {
    x.className = x.className.replace("showBtn", "closeBtn");
  }
};

// event search product list
const searchProduct = async () => {
  var request = document.getElementById("searchNav").value.trim();
  console.log(request);
  if (!request) {
    return;
  } else {
    document.getElementById("searchNav").value = "";
    const dataSearch = listProduct.filter((obj) =>
      obj.name.toLowerCase().includes(request)
    );
    document.getElementById("body").innerHTML = `
    <section id="productCart" class="container">
            <h1 id="label">MẪU XE ĐẶT TRƯNG</h1>
            <div id="listCart">
                ${renderProductList(dataSearch)}
            </div>
        </section>
    `;
  }
};

// event search product navbar Collapse list
const searchProductNavCollapse = async () => {
  var request = document.getElementById("searchNavCollapse").value.trim();
  console.log(request);
  if (!request) {
    return;
  } else {
    document.getElementById("searchNavCollapse").value = "";
    const dataSearch = listProduct.filter((obj) =>
      obj.name.toLowerCase().includes(request)
    );
    document.getElementById("body").innerHTML = `
    <section id="productCart" class="container">
            <h1 id="label">MẪU XE ĐẶT TRƯNG</h1>
            <div id="listCart">
                ${renderProductList(dataSearch)}
            </div>
        </section>
    `;
  }
};

// event received sales from detail
const InfoSales = () => {
  document.getElementById("detailInfoSales").innerText = "/ -10%";
};

// event handle send contact
const handleSendContact = () => {
  alert(
    "Cảm ơn bạn đã gửi liên hệ với chúng tôi. Chúng tôi sẽ liên hệ với bạn sớm nhất có thể!"
  );
};

// event sign in
const signIn = async () => {
  var username = document.getElementById("txtName").value;
  var password = document.getElementById("txtPassword").value;
  // console.log(value)
  var isValid = checkNull("spanName", username);
  isValid = isValid & checkNull("spanPass", password);
  if (!isValid) {
    return;
  }
  const user = listUser.find(
    ({ name, password }) => name === username && password === password
  );
  if (!user) {
    return;
  } else {
    alert("Bạn đã đăng nhập thành công!");
    storageServices.setDataUser(user);
  }
};

// object CartItem
class CartItem {
  constructor(name, image, price, type, detail, color, quantity) {
    this.name = name;
    this.image = image;
    this.price = price;
    this.type = type;
    this.detail = detail;
    this.color = color;
    this.quantity = quantity;
  }
}

// product cart
var cart = [];
var arrayCart = storageServices.getDataProduct();
// console.log(arrayCart);
if (arrayCart != null) {
  for (var i = 0; i < arrayCart.length; i++) {
    var item = arrayCart[i];
    var productCart = new CartItem(
      item.name,
      item.image,
      item.price,
      item.type,
      item.detail,
      item.color,
      item.quantity
    );
    cart.push(productCart);
    // console.log("cart", cart);
  }
}
// event handle decrease / increase quantity
const upDownQuanlity = (event, id) => {
  console.log(event);
  switch (event) {
    case "down":
      downCart(id);
      break;
    case "up":
      upCart(id);
      break;
    default:
      break;
  }
};
// decrease quantity
const downCart = (id) => {
  const index = cart.findIndex((item) => {
    if (item.id == id) {
      return true;
    }
  });
  cart[index].quantity--;
  console.log(cart[index]);
  fetchProductCart();
  // renderProductCart(arrayCart);
  if (cart[index].quantity === 0) {
    cartItemService
      .remove(id)
      .then((res) => {
        fetchProductCart();
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    return;
  }
  cartItemService
    .update(id, cart[index])
    .then((res) => {
      fetchProductCart();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
// increase quantity
const upCart = (id) => {
  const index = cart.findIndex((item) => {
    if (item.id == id) {
      return true;
    }
  });
  cart[index].quantity++;
  cartItemService
    .update(id, cart[index])
    .then((res) => {
      fetchProductCart();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
// add to cart
const addToCart = async (id) => {
  const dataDetail = listProduct.find((obj) => obj.id == id);
  const index = cart.findIndex((item) => {
    if (item.name === dataDetail.name) {
      return true;
    }
  });
  if (index !== -1) {
    const id = cart[index].id;
    cart[index].quantity++;
    cartItemService
      .update(id, cart[index])
      .then((res) => {
        fetchProductCart();
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    return;
  }
  var cartItem = new CartItem(
    dataDetail.name,
    dataDetail.image,
    dataDetail.price,
    dataDetail.type,
    dataDetail.detail,
    dataDetail.color,
    1
  );
  cartItemService
    .create(cartItem)
    .then((res) => {
      fetchProductCart();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
// remove product from cart
const removeCart = (id) => {
  cartItemService
    .remove(id)
    .then((res) => {
      fetchProductCart();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

// event payment method
const handlePayment = () => {
  const dataUserAccount = storageServices.getDataUser();
  if (!dataUserAccount) {
    alert("Hãy đăng nhập tài khoản trước khi thanh toán!");
    return;
  }
  if (cart.length === 0) {
    alert("Your cart is empty..");
    return;
  }
  handleDeleteAll();
  alert("Thank you for your purchase!");
};

// delete all products from cart
const handleDeleteAll = async () => {
  try {
    for (let index = 0; index < cart.length; index++) {
      const id = cart[index].id;
      await cartItemService.remove(id);
    }
    fetchProductCart();
    storageServices.removeTotalCart();
    storageServices.removeDataProduct();
  } catch (error) {
    console.log(error);
  }
};

// fetch product to get cart
const fetchProductCart = async () => {
  cartItemService
    .getList()
    .then((res) => {
      cart = res.data;
      console.log(res);
      renderProductCart(cart);
      storageServices.setDataProduct(cart);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProductCart();
var arrSort = [];
const sortPriceProduct = (value, event) => {
  document.querySelector(".array1").style.display = "none";
  console.log(value, event);
  const dataValue = listProduct.filter((obj) => obj.type === value);
  const parsePrice = (price) => parseInt(price.replace(/\./g, ""), 10);
  switch (event) {
    case "ascending":
      arrSort = dataValue.sort(
        (a, b) => parsePrice(a.price) - parsePrice(b.price)
      );
      console.log(arrSort);
      document.querySelector(".array2").innerHTML = renderProductList(arrSort);
      break;
    case "descending":
      arrSort = dataValue.sort(
        (a, b) => parsePrice(b.price) - parsePrice(a.price)
      );
      console.log(arrSort);
      document.querySelector(".array2").innerHTML = renderProductList(arrSort);
    default:
      break;
  }
  return arrSort;
};
