/** @format */

// function render Home page
const HomePage = async () => {
  document.getElementById("body").innerHTML = `
  <section id="special" class="container">
        <h1 id="label">MẪU XE NỔI BẬT</h1>
        <div id="specialContent">
            <div id="specialProduct">
              <div class="specialProMenu">
                <label for="slide-dot-1"></label>
                <label for="slide-dot-2"></label>
                <label for="slide-dot-3"></label>
                <label for="slide-dot-4"></label>
              </div>
                ${renderSpecialProduct(listProduct)}
            </div>
            <div id="specialContentContext">
                <p>
                    Cửa hàng Kawasaki Group 4 với khát vọng chọn lọc
                    và đem đến cho khách hàng những sản phảm mới, nổi bật HOT
                    hiện nay.
                </p>
                <p>
                    Những cách thức cần thiết khi mua hàng:
                <ul>
                    <li>
                        _ Trước tiên bạn vào trang Login để đăng nhập tài khoản.
                    </li>
                    <li>
                        _ Sau đó bạn có thể chọn những sản phẩm từ chính trang chủ hay theo từng
                        danh mục xe mà bạn thích, sau đó sẽ hiện ra thông tin chi
                        tiết của sản phẩm và có nút mua hàng và bạn chỉ cần chọn vào nó.
                        Bạn đã thành công thêm vào giỏ hàng.
                    </li>
                    <li>
                        _ Tiếp theo bạn vào trang giỏ hàng để kiểm tra lại đúng những sản phẩm
                        cần mua (tên sản phẩm, số lượng, đơn giá,...).
                    </li>
                    <li>
                        _ Và cuối cùng bạn hãy cung cấp cho chúng tôi thông tin liên lạc gồm có:
                        họ và tên, số điện thoại, địa chỉ giao hàng. Hoàn tất và thanh toán.
                    </li>
                </ul>
                </p>
            </div>
        </div>
    </section> 
        <section id="productCart" class="container">
            <h1 id="label">MẪU XE ĐẶC TRƯNG</h1>
            <div id="listCart">
              ${renderProductListHomePage(listProduct)}
            </div>
            <div id="load"><button id="btnViewMore">Xem Thêm</button></div> 
        </section>
        <section id="about" class="container">
          <h1 id="label">VỀ CHÚNG TÔI</h1>
          <p id="aboutInfo">
            Kawasaki Group 4 Shop, Chúng tôi vừa mới thành lập được chi nhánh đầu tiên bán lẻ xe Kawasaki. 
            Chúng tôi luôn hoan nghênh quý khách đến tham quan, chiêm ngưỡng và điều đặc biệt nhất quý khách hàng có thể 
            biến những mẫu xe nổi bật độc đáo trên thị trường từ nhà phân phối Kawasaki nằm trong bộ sưu tập siêu xe của 
            mình. Và làm để làm được điều đó shop chúng tôi cùng với đội ngũ nhân viên vô cùng tâm huyết để cùng nhau gây
            dựng nên điều tuyệt vời ngày hôm nay.
          </p>
          <div id="aboutGroup">
            ${renderGroupMember()}
          </div>
        </section>
  `;
};
HomePage();

// function render Contact Page
const ContactPage = () => {
  document.getElementById("body").innerHTML = `
  <section id="contact" class="container">
      <h1 id="label">LIÊN HỆ VỚI CHÚNG TÔI</h1>
      <div id="contactContent">
        <div id="contact-form">
          <form onsubmit="handleSendContact()">
            <div>
              <label for="name">Họ và Tên</label>
              <input type="text" id="name" name="name" required />
            </div>
            <div>
              <label for="email">Email</label>
              <input type="text" id="email" name="email" required/>
            </div>
            <div>
              <label for="phone">SĐT</label>
              <input type="tel" id="phone" name="phone" required/>
            </div>
            <div>
              <label for="mess">Lời Nhắn</label>
              <textarea name="mess" id="mess" rows="6" required></textarea>
            </div>
            <div>
              <button type="submit">Gửi Liên Hệ</button>
            </div>
          </form>
        </div>
        <div id="contact-map">
          <map name="">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.4150499198827!2d106.62725477451814!3d10.85600285772194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529deaaaaaaab%3A0xce800a25143c8e3a!2zQ2FvIMSQ4bqzbmcgU8OgaSBHw7Ju!5e0!3m2!1svi!2s!4v1713416742145!5m2!1svi!2s"
              width="600"
              height="400"
              style="border: 0"
              allowfullscreen=""
              loading="lazy"
              referrerpolicy="no-referrer-when-downgrade"></iframe>
          </map>
          <div id="contactInfo">
            <p>
              Địa chỉ: Công viên Phần mềm Quang Trung, Tòa nhà SaigonTech, Lô 14
              Đường Số 5, Tân Chánh Hiệp, Quận 12, Thành phố Hồ Chí Minh .
            </p>
            <p>Email: admissions@saigontech.edu.vn</p>
            <p>SĐT: (028) 37 155 033</p>
          </div>
        </div>
      </div>
    </section>
  `;
};

// function render Detail page
const DetailPage = async (id) => {
  const dataDetail = listProduct.find((obj) => obj.id == id);
  const dataType = listProduct.filter((obj) => obj.type == dataDetail.type);
  document.getElementById("body").innerHTML = `
   <section id="detailProduct" class="container">
      <div id="detailContent">
        <div style="display: block; margin: 0 auto;">
          <img src=${dataDetail.image} alt=${dataDetail.name}>
        </div>
        <div id="detailInfo">
          <h1>${dataDetail.name}</h1>
          <p>Mã Sản Phẩm: ${dataDetail.id}</p>
          <p>Xuất Xứ: Nhật Bản</p>
          <p>Loại Sản Phẩm: ${dataDetail.type}</p>
          <p>Tình Trạng: Còn</p>
          <p>Màu Sắc: ${dataDetail.color}</p>
          <p>Giá: <span style="color: red; font-weight: 600;">${
            dataDetail.price
          }đ <span id="detailInfoSales"></span></span></p>
          <button type="button" onclick="InfoSales()">Nhận Ưu Đãi</button>
        </div>
      </div>
      <div id="detailFullInfo">
        <h1>CHI TIẾT SẢN PHẨM</h1>
        <h3>KAWASAKI ${dataDetail.name} CHÍNH THỨC RA MẮT</h3>
        <img src=${dataDetail.image} alt=${dataDetail.name}>
        <h6><i>KAWASAKI ${dataDetail.name} Phiên Bản Màu ${
    dataDetail.color
  }</i></h6>
        <h5>KAWASAKI ${
          dataDetail.name
        } sở hửu khối động cơ với sức mạnh ra sao?</h5>
        <p>${dataDetail.detail}</p>
        <div style="text-align: right;">
          <button type="button" onclick="addToCart('${
            dataDetail.id
          }')">Mua Ngay</button>
        </div>
      </div>
    </section> 
  <section id="productCart" class="container">
            <h1 id="label">MẪU XE CÙNG LOẠI</h1>
            <div id="listCart">
              ${renderProductList(dataType)}
            </div>
  </section>
        `;
};

// function render Cart page
const CartPage = () => {
  // sessionStorage.setItem("test", "name test");
  const dataCart = storageServices.getDataProduct();
  const total = storageServices.getTotalCart();
  console.log(total);
  document.getElementById("body").innerHTML = `
    <section id="cartPage" class="container">
      <h1 id="label">CHI TIẾT ĐƠN HÀNG</h1>
      <div id="cartPageContent">
        <div>
          <p>
            ${total == 0 ? "Giỏ Hàng Của Bạn Đang Trống Hãy Mua Hàng Nhé." : ""}
          </p>
          ${renderProductCart(dataCart)}
          <p>
            Tổng Đơn Hàng:
            <span style="color: red; font-weight: 600">${total}đ</span>
          </p>
        </div>
        <div id="cartForm">
          <h1>THÔNG TIN GIAO HÀNG</h1>
          <form onsubmit="handlePayment()">
            <div>
              <label for="username">Họ và Tên</label>
              <input type="text" name="username" id="txtUsername" required />
            </div>
            <div>
              <label for="phone">Số Điện Thoại</label>
              <input type="tel" name="phone" id="txtPhone" required />
            </div>
            <div>
              <label for="address">Địa Chỉ</label>
              <input type="tel" name="address" id="txtAddress" required />
            </div>
            <div>
              <label for="note">Ghi Chú</label>
              <textarea type="tel" name="note" id="note" row="5"></textarea>
            </div>
            <button type="submit">Thanh Toán</button>
          </form>
        </div>
      </div>
    </section>
  `;
};

// function render Login page
const LoginPage = async () => {
  document.getElementById("body").innerHTML = `
        <section id="login-page">
          <div id="form">
          <form>
            <label>Chào Mừng Trở Lại Quý Khách</label>
            <label for="name">Tài Khoản:</label>
            <input type="text" name="txtName" id="txtName" value="Tri" placeholder="Your Name">
            <span id="spanName"></span>
            <label for="password">Mật Khẩu:</label>
            <input type="password" name="txtPassword" id="txtPassword" value="1234" placeholder="Your Pass">
            <span id="spanPass"></span>
            <button type="button" onclick="signIn()">Login</button>
          </form>
          </div>
        </section>
    `;
};
